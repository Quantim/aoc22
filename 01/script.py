#!/usr/bin/env python3
file1 = open('input', 'r')
count = 0
elves = []

for line in file1:
    if not line.strip():
        elves.append(count)
        count = 0
        continue
    count += int(line)

elves.sort(reverse=True)

print(elves[0]+elves[1]+elves[2])
