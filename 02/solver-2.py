#!/usr/bin/env python3
file1 = open('input', 'r')
score = 0

for line in file1:
        line = line.strip()
        match line:
            case "A X":
                score += 3 + 0
            case "A Y":
                score += 1 + 3
            case "A Z":
                score += 2 + 6
            case "B X":
                score += 1 + 0
            case "B Y":
                score += 2 + 3
            case "B Z":
                score += 3 + 6
            case "C X":
                score += 2 + 0
            case "C Y":
                score += 3 + 3
            case "C Z":
                score += 1 + 6



print(score)
