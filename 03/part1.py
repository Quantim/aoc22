#!/usr/bin/env python3
file1 = open('input', 'r')
result = 0
num = 0

for line in file1:
    line = line.strip()
    firstpart, secondpart = line[:len(line)//2], line[len(line)//2:]
    for char in firstpart:
        if char in secondpart:
            print(char)
            if char == char.lower():
                num = ord(char) - 96
            else:
                num = ord(char) - 38
            print(num)
            result += num
            break

print(result)


