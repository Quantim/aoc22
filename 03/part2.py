#!/usr/bin/env python3
file1 = open('input', 'r')
result = 0
num = 0
counter = 0

lines = ["","",""]

for line in file1:
    lines[counter] = line.strip()
    counter += 1
    if counter == 3:
        counter = 0
        for char in lines[0]:
            if char in lines[1]:
                if char in lines[2]:
                    if char == char.lower():
                        num = ord(char) - 96
                    else:
                        num = ord(char) - 38
                    result += num
                    break

print(result)


