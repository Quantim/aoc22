#!/usr/bin/env python3
file1 = open('input', 'r')
counter1 = 0
counter2 = 0


for line in file1:
    one,two = line.strip().split(",")
    one_start, one_stop = one.split("-")
    two_start, two_stop = two.split("-")
    one = set(range(int(one_start),int(one_stop)+1))
    two = set(range(int(two_start),int(two_stop)+1))
    if one.issuperset(two) or two.issuperset(one):
        counter1 += 1
    if one.intersection(two):
        counter2 += 1
print(counter1)
print(counter2)



