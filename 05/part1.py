#!/usr/bin/env python3

import queue
plan = [None] * 10
buffer = queue.LifoQueue()

def arrange(steps, source, destination):
    for i in range(0, steps):
        plan[destination].put(plan[source].get())

def arrange2(steps, source, destination):
    for i in range(0, steps):
        buffer.put(plan[source].get())
    while not buffer.empty():
        plan[destination].put(buffer.get())



file1 = open('input', 'r')


for i in range(0,10):
    plan[i] = queue.LifoQueue()

#list(map(plan[1].put, ["Z", "N"] ))
#list(map(plan[2].put, ["M", "C", "D"]))
#list(map(plan[3].put, ["P"]))

list(map(plan[1].put, ["F", "C", "P", "G", "Q", "R"]))
list(map(plan[2].put, ["W", "T", "C", "P"] ))
list(map(plan[3].put, ["B", "H", "P", "M", "C"] ))
list(map(plan[4].put, ["L", "T", "Q", "S", "M", "P", "R"] ))
list(map(plan[5].put, ["P", "H", "J", "Z", "V", "G", "N"] ))
list(map(plan[6].put, ["D", "P", "J"] ))
list(map(plan[7].put, ["L", "G", "P", "Z", "F", "J", "T", "R"] ))
list(map(plan[8].put, ["N", "L", "H", "C", "F", "P", "T", "J"] ))
list(map(plan[9].put, ["G", "V", "Z", "Q", "H", "T", "C", "W"] ))



for line in file1:
    steps, source, destination = [int(s) for s in line.split() if s.isdigit()]
    arrange2(steps, source, destination)



for i in range(1,10):
    print(plan[i].get(), end = '')
#print(plan[1].get(), plan[2].get(), plan[3].get())
print()


