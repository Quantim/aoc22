#!/usr/bin/env python3

file1 = open('input', 'r')
heightmap = []
rows = 0
cols = 0

visible = 0


def check_visible(i, j):
    tree = heightmap[i][j]
    col_slice = [heightmap[i][j] for i in range(0,rows)]
    if max(heightmap[i][0:j]) < tree:
        return True
    if max(heightmap[i][j+1:cols]) < tree:
        return True
    if max(col_slice[0:i]) < tree:
        return True
    if max(col_slice[i+1:rows]) < tree:
        return True
    return False



for line in file1:
    row = []
    for i in range(0,len(line.strip())):
        row.append(line[i])
    heightmap.append(row)

rows = len(heightmap)
cols = len(heightmap[0])

for i in range(rows):
    if i == 0 or i == rows - 1:
        visible += cols
        continue
    for j in range(cols):
        if j == 0 or j == cols - 1:
            visible += 1
            continue
        if check_visible(i, j):
            visible += 1

print(visible)





