#!/usr/bin/env python3

file1 = open('input', 'r')
heightmap = []
rows = 0
cols = 0
max_score = 1

visible = 0


def calculate_score(i, j):
    tree = heightmap[i][j]
    col_slice = [heightmap[i][j] for i in range(0,rows)]
    row_slice = heightmap[i]
    score = 1
    top = True

    for k in reversed(range(j)):
        if row_slice[k] >= tree:
            score = score * (j - k)
            top = False
            break
    if (top):
        score = score * j
    top = True
    for k in range(j+1,cols):
        if row_slice[k] >= tree:
            score = score * (k - j)
            top = False
            break
    if (top):
        score = score * (cols - j - 1)
    top = True
    for k in reversed(range(i)):
        if col_slice[k] >= tree:
            score = score * (i - k)
            top = False
            break
    if (top):
        score = score * i
    top = True
    for k in range(i+1, rows):
        if col_slice[k] >= tree:
            score = score * (k - i)
            top = False
            break
    if(top):
        score = score * (rows - i - 1)
    return score



for line in file1:
    row = []
    for i in range(0,len(line.strip())):
        row.append(line[i])
    heightmap.append(row)

rows = len(heightmap)
cols = len(heightmap[0])

for i in range(rows):
    for j in range(cols):
        max_score = max(max_score,calculate_score(i, j))


print(max_score)



