#!/usr/bin/env python3

import math

file1 = open('input', 'r')
#array = [ [0] * 1000 for _ in range(1000)]

head_pos = [500,500]
tail_pos = [500,500]

results = set()

visible = 0


def calculate_tail():
    print(head_pos, tail_pos)
    print(math.dist(head_pos, tail_pos))
    if (math.dist(head_pos, tail_pos) > 1.5):
        if abs(head_pos[0] - tail_pos[0]) == 2:
            tail_pos[0] += int((head_pos[0] - tail_pos[0])/2)
            tail_pos[1] = head_pos[1]
        else:
            tail_pos[0] = head_pos[0]
            tail_pos[1] += int((head_pos[1] - tail_pos[1])/2)

            

            
    print(head_pos, tail_pos)
    print("---------")
#    array[tail_pos[0]][tail_pos[1]] = 1
    results.add(tuple(tail_pos))



for line in file1:
    direction,steps = (line.strip()).split()
    steps = int(steps)
    print(direction)
    match direction:
        case "R":
            for i in range(steps):
                head_pos[0] += 1
                calculate_tail()
        case "L":
            for i in range(steps):
                head_pos[0] -= 1
                calculate_tail()
        case "U":
            for i in range(steps):
                head_pos[1] -= 1
                calculate_tail()
        case "D":
            for i in range(steps):
                head_pos[1] += 1
                calculate_tail()

print(head_pos)
#print(sum(sum(array, [])))
print(len(results))




