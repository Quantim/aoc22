#!/usr/bin/env python3

import math

file1 = open('input', 'r')
array = [ [0] * 1000 for _ in range(1000)]

pos = []
for i in range(10):
    pos.append([500,500])



def print_debug():
    
    for y in range(490,501):
        for x in range(500,510):
            try:
                index = pos.index([x,y])
                print(index, end='')
            except:
                print(".", end='')

        print()



def calculate_tail():
    for i in range(9):
        if (math.dist(pos[i], pos[i+1]) > 1.5):
            if abs(pos[i][0] - pos[i+1][0]) == 2:
                pos[i+1][0] += int((pos[i][0] - pos[i+1][0])/2)
                if pos[i+1][1] != pos[i][1]:
                    if abs(pos[i][1] - pos[i+1][1]) == 2:
                        pos[i+1][1] += int((pos[i][1] - pos[i+1][1])/2)
                    else:
                        pos[i+1][1] = pos[i][1]
            else:
                pos[i+1][1] += int((pos[i][1] - pos[i+1][1])/2)
                if pos[i+1][0] != pos[i][0]:
                    if abs(pos[i][0] - pos[i+1][0]) == 2:
                        pos[i+1][0] += int((pos[i][0] - pos[i+1][0])/2)
                    else:
                        pos[i+1][0] = pos[i][0]
    
            

    array[pos[9][0]][pos[9][1]] = 1



for line in file1:
    direction,steps = (line.strip()).split()
    steps = int(steps)
    match direction:
        case "R":
            for i in range(steps):
                pos[0][0] += 1
                calculate_tail()
        case "L":
            for i in range(steps):
                pos[0][0] -= 1
                calculate_tail()
        case "U":
            for i in range(steps):
                pos[0][1] -= 1
                calculate_tail()
        case "D":
            for i in range(steps):
                pos[0][1] += 1
                calculate_tail()

print(sum(sum(array, [])))



