#!/usr/bin/env python3

file1 = open('input', 'r')
cycle = 0
X = 1
prev_X = 1

datalog = [0] * 260


for line in file1:
    if line.startswith("a"):
        _,value = line.strip().split()
        value = int(value)
        datalog[cycle + 1] = prev_X = X
        X += value
        cycle += 2
    else:
        cycle += 1
    datalog[cycle] = X 

    print(cycle, X)

signal_strength = 0
result = 0

for i in [20, 60, 100, 140, 180, 220]:
    print(datalog[i -1] * i)
    signal_strength = datalog[i -1] * i
    result += signal_strength


print (result)


