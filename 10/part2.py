#!/usr/bin/env python3

file1 = open('input', 'r')
cycle = 0
X = 1
prev_X = 1

datalog = [0] * 260


for line in file1:
    if line.startswith("a"):
        _,value = line.strip().split()
        value = int(value)
        datalog[cycle + 1] = prev_X = X
        X += value
        cycle += 2
    else:
        cycle += 1
    datalog[cycle] = X 


for i in range(260):
#    print(i, datalog[i])
    if i%40 == 0:
        print()
    if datalog[i] - 1 <= i%40 and i%40 <= datalog[i] + 1:
        print("#", end = '')
    else:
        print(".", end = '')




